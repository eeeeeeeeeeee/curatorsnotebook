#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql/QSqlTableModel>
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include "authdialog.h"
#include "changedialog.h"
#include "adddialog.h"

#define TABLE_STUDENTS 0
#define TABLE_ACTIVITY 1
#define TABLE_ACTIVITY_JOURNAL 2
#define TABLE_SUBJECTS 3
#define TABLE_SUBJECTS_JOURNAL 4
#define TABLES 4

extern int allow_root;
extern QSqlDatabase db;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_3_clicked();
    void on_pushButton_2_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_pushButton_4_clicked();

    void on_action_3_triggered();

    void on_action_triggered();
    void showEvent ( QShowEvent * event );

    void on_action_2_triggered();

    void on_action_4_triggered();

private:
    Ui::MainWindow *ui;
    QSqlTableModel *model[5];
    AuthDialog* w;
    QDialog* cw;

    void updateUi();
    void updateComboBox(int);
    void setPermissions();
};

#endif // MAINWINDOW_H
