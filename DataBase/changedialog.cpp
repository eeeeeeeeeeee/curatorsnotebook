#include "changedialog.h"
#include "ui_changedialog.h"

ChangeDialog::ChangeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangeDialog) {
    ui->setupUi(this);

    QSqlQuery query;
    QString q = "SELECT * FROM users WHERE id = ";
    q.append(QString::number(user_id));
    q.append(";");

    query.exec(q);
    query.next();

    QString text = "Hello, ";
    text.append(query.value(1).toString());
    ui->label->setText(text);
}

ChangeDialog::~ChangeDialog() {
    delete ui;
}

void ChangeDialog::on_pushButton_clicked() {

    QSqlQuery query;
    QString q = "SELECT * FROM users WHERE id = ";
    q.append(QString::number(user_id));
    q.append(";");

    query.exec(q);
    query.next();

    QString cq = "Hello, ";
    if(ui->lineEdit->text() == query.value(2).toString()) {
        if(ui->lineEdit_2->text() == ui->lineEdit_3->text() && ui->lineEdit_2->text() != "") {
            QSqlQuery chquery;
            chquery.prepare("UPDATE users SET password=:pass WHERE id=:id");
            chquery.bindValue(":id", user_id);
            chquery.bindValue(":pass", ui->lineEdit_2->text());
            chquery.exec();
            close();
        } else {
            ui->label->setText("Пароли не совпадают");
        }
    } else {
        ui->label->setText("Некорректный пароль пользователя");
    }
}

void ChangeDialog::on_pushButton_2_clicked() {
    close();
}
