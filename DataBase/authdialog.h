#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QDialog>
#include <QtSql/QSqlTableModel>
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QMainWindow>
#include <QMessageBox>
#include <QByteArray>
#include <QFile>

extern int allow_root;
extern int user_id;
extern QSqlDatabase db;

namespace Ui { class AuthDialog;
}

class AuthDialog : public QDialog {
    Q_OBJECT

public:
    explicit AuthDialog(QWidget *parent = nullptr);
    void setParent(QMainWindow* w);
    ~AuthDialog();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::AuthDialog *ui;
    QMainWindow* w;
};

#endif // AUTHDIALOG_H
