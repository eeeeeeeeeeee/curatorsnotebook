#include "authdialog.h"
#include "ui_authdialog.h"

AuthDialog::AuthDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthDialog) {
    ui->setupUi(this);

    QFile file("../data/pass.cfg");
    if(file.open(QIODevice::ReadOnly)) {
        QString text;
        QString field[2];

        QByteArray ba;
        ba = file.readAll();
        ba = QByteArray::fromBase64(ba);
        text = ba;

        int f = 0;
        for(int i=0;i<ba.length();i++) {
            if(text[i]=='\n') {
                f++;
                continue;
            }
            field[f].append(text[i]);
        }

        ui->lineEdit->setText(field[0]);
        ui->lineEdit_2->setText(field[1]);
    }
}

void AuthDialog::setParent(QMainWindow* w) {
   this->w = w;
}

AuthDialog::~AuthDialog() {
    delete ui;
}

void AuthDialog::on_pushButton_clicked() {
    QSqlQuery query;

    query.exec("SELECT * FROM users;");

    for(int i=0;query.next();i++) {
        if(query.value(1).toString() == ui->lineEdit->text() &&
                query.value(2).toString() == ui->lineEdit_2->text()) {


            if(query.value(3).toInt() > 0) allow_root = 1; else allow_root = 0;
            ui->label_3->setText("");
            user_id = query.value(0).toInt();

            w->show();
            this->hide();

            //QString src = "Hello";
            //src.toUtf8().toBase64();

            QFile file("../data/pass.cfg");
            if(file.open(QIODevice::WriteOnly)) {
                QString login = ui->lineEdit->text();
                QString pass = ui->lineEdit_2->text();

                login.append("\n");

                QByteArray ba;
                ba.append(login);
                ba.append(pass);

                qDebug() << ba;
                ba = ba.toBase64();
                qDebug() << ba;

                file.write(ba, ba.length());
            }

            return;
        }
    }

    ui->label_3->setText("неверный логин или пароль");
}

void AuthDialog::on_pushButton_2_clicked() {
    exit(0);
}

void AuthDialog::on_pushButton_3_clicked() {
    QSqlQuery query;

    query.exec("SELECT * FROM users;");

    for(int i=0;query.next();i++) {
        if(query.value(1).toString() == ui->lineEdit->text()) {
            QMessageBox::about(this, "Hint", query.value(4).toString());
            return;
        }
    }
    QMessageBox::about(this, "Hint", "Такого пользователя нет");
}
