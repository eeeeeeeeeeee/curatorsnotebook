#ifndef CHANGEDIALOG_H
#define CHANGEDIALOG_H

#include <QDialog>
#include <QDialog>
#include <QtSql/QSqlTableModel>
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>

extern int allow_root;
extern int user_id;
extern QSqlDatabase db;

namespace Ui {
class ChangeDialog;
}

class ChangeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChangeDialog(QWidget *parent = nullptr);
    ~ChangeDialog();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::ChangeDialog *ui;
};

#endif // CHANGEDIALOG_H
