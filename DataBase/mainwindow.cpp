#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("../data/database.db");
    db.open();

    for(int i=0;i<=TABLES;i++) {
        model[i] = new QSqlTableModel(this);
    }

    model[TABLE_STUDENTS]->setTable("students");
    model[TABLE_SUBJECTS]->setTable("subject_to_group");
    model[TABLE_SUBJECTS_JOURNAL]->setTable("subject_journal");
    model[TABLE_ACTIVITY]->setTable("activity_to_group");
    model[TABLE_ACTIVITY_JOURNAL]->setTable("activity_journal");

    for(int i=0;i<=TABLES;i++) {
        model[i]->select();
    }

     ui->tableView->setModel(model[TABLE_STUDENTS]);
     ui->tableView_2->setModel(model[TABLE_SUBJECTS]);
     ui->tableView_3->setModel(model[TABLE_SUBJECTS_JOURNAL]);
     ui->tableView_4->setModel(model[TABLE_ACTIVITY]);
     ui->tableView_5->setModel(model[TABLE_ACTIVITY_JOURNAL]);

     ui->tableView->setSelectionMode(QAbstractItemView::NoSelection);
     ui->tableView_2->setSelectionMode(QAbstractItemView::NoSelection);
     ui->tableView_3->setSelectionMode(QAbstractItemView::NoSelection);
     ui->tableView_4->setSelectionMode(QAbstractItemView::NoSelection);
     ui->tableView_5->setSelectionMode(QAbstractItemView::NoSelection);

     updateComboBox(0);

     w = new AuthDialog(this);
     w->setParent(this);
     w->show();

}

MainWindow::~MainWindow() {
    delete ui;
}


void MainWindow::showEvent( QShowEvent * event ) {
    qDebug() << "Show";
    setPermissions();
}


void MainWindow::setPermissions() {
    if(!allow_root) {
        ui->action_4->setDisabled(1);

        ui->pushButton_2->setDisabled(1);
        ui->pushButton_3->setDisabled(1);

        ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->tableView_2->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->tableView_3->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->tableView_4->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->tableView_5->setEditTriggers(QAbstractItemView::NoEditTriggers);
    } else {
        ui->action_4->setEnabled(1);

        ui->pushButton_2->setEnabled(1);
        ui->pushButton_3->setEnabled(1);

        ui->tableView->setEditTriggers(QAbstractItemView::AllEditTriggers);
        ui->tableView_2->setEditTriggers(QAbstractItemView::AllEditTriggers);
        ui->tableView_3->setEditTriggers(QAbstractItemView::AllEditTriggers);
        ui->tableView_4->setEditTriggers(QAbstractItemView::AllEditTriggers);
        ui->tableView_5->setEditTriggers(QAbstractItemView::AllEditTriggers);
    }
}

void MainWindow::updateUi() {
    for(int i=0;i<=TABLES;i++) {
        model[i]->select();
    }
}

void MainWindow::updateComboBox(int index) {
    QSqlQuery query;

    switch(index) {
        case 0 : {
            query.prepare("PRAGMA table_info(students);");
            break;
        }
        case 1 : {
            query.prepare("PRAGMA table_info(subject_to_group);");
            break;
        }
        case 2 : {
            query.prepare("PRAGMA table_info(subject_journal);");
            break;
        }
        case 3 : {
            query.prepare("PRAGMA table_info(activity_to_group);");
            break;
        }
        default: {
            query.prepare("PRAGMA table_info(activity_journal);");
            break;
        }
    }

    if(!query.exec()) {
        qDebug() << query.lastQuery();
        qDebug() << query.lastError().text();
        return;
    }

    ui->comboBox->clear();

    while(query.next()) {
        QString column = query.value(1).toString();
        ui->comboBox->addItem(column);
    }
}

// Удаление выбранной строки из таблицы
void MainWindow::on_pushButton_3_clicked() {
    if(!allow_root) return;

    QSqlQuery query;
    QTableView *view;
    switch (ui->tabWidget->currentIndex()) {
        case 1 : {
            query.prepare("DELETE FROM subject_to_group WHERE key = :id;");
            view = ui->tableView_2;
            break;
        }
        case 2 : {
            query.prepare("DELETE FROM subject_journal WHERE key = :id;");
            view = ui->tableView_3;
            break;
        }
        case 3 : {
            query.prepare("DELETE FROM activity_to_group WHERE activity_id = :id;");
            view = ui->tableView_4;
            break;
        }
        case 4 : {
            query.prepare("DELETE FROM activity_journal WHERE key = :id;");
            view = ui->tableView_5;
            break;
        }
        default : {
            query.prepare("DELETE FROM students WHERE student_id = :id;");
            view = ui->tableView;
            break;
        }
    }

    int row = view->currentIndex().row();
    int id = view->model()->data( view->model()->index(row, 0) ).toInt();

    query.bindValue(":id", id);

    if(!query.exec()) {
        qDebug() << query.lastQuery();
        qDebug() << query.lastError().text();
    };

    updateUi();
}

// Добавление новой строки в таблицу
void MainWindow::on_pushButton_2_clicked() {
    if(!allow_root) return;

    QSqlQuery query;
    switch (ui->tabWidget->currentIndex()) {
        case 1 : {
            query.prepare("INSERT INTO subject_to_group DEFAULT VALUES;");
            break;
        }
        case 2 : {
            query.prepare("INSERT INTO subject_journal DEFAULT VALUES;");
            break;
        }
        case 3 : {
            query.prepare("INSERT INTO activity_to_group DEFAULT VALUES;");
            break;
        }
        case 4 : {
            query.prepare("INSERT INTO activity_journal DEFAULT VALUES;");
            break;
        }
        default : {
            query.prepare("INSERT INTO students DEFAULT VALUES;");
            break;
        }
    }

    if(!query.exec()) {
        qDebug() << query.lastQuery();
        qDebug() << query.lastError().text();
    };

    updateUi();
}

void MainWindow::on_tabWidget_currentChanged(int index) {
    updateComboBox(index);
}

void MainWindow::on_pushButton_4_clicked() {
    QSqlQuery query;
    QString q = "SELECT * FROM ";

    switch (ui->tabWidget->currentIndex()) {
        case 1 : {
            q.append("subject_to_group");
            break;
        }
        case 2 : {
            q.append("subject_journal");
            break;
        }
        case 3 : {
            q.append("activity_to_group");
            break;
        }
        case 4 : {
            q.append("activity_journal");
            break;
        }
        default : {
            q.append("students");
            break;
        }
    }

    q.append(" WHERE ");
    q.append(ui->comboBox->itemText(ui->comboBox->currentIndex()));
    q.append(" = ");
    q.append(ui->lineEdit->text());
    q.append(";");

    query.exec(q);

    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(3);
    for(int i=0;query.next();i++) {
        ui->tableWidget->insertRow(i);
        ui->tableWidget->setItem(i,0, new QTableWidgetItem(query.value(0).toString()));
        ui->tableWidget->setItem(i,1, new QTableWidgetItem(query.value(1).toString()));
        ui->tableWidget->setItem(i,2, new QTableWidgetItem(query.value(2).toString()));
    }
}

void MainWindow::on_action_3_triggered() {
    exit(0);
}

void MainWindow::on_action_triggered() {
    this->hide();
    allow_root = 0;
    w->show();
}

void MainWindow::on_action_2_triggered() {
    if(cw!=nullptr) {
        cw = new ChangeDialog(this);
        cw->show();
    }
}

void MainWindow::on_action_4_triggered() {
    cw = new AddDialog(this);
    cw->show();
}
