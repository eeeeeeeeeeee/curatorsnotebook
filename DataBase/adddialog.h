#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include <QtSql/QSqlTableModel>
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QMainWindow>

extern int allow_root;
extern int user_id;
extern QSqlDatabase db;

namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialog(QWidget *parent = nullptr);
    ~AddDialog();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::AddDialog *ui;
};

#endif // ADDDIALOG_H
