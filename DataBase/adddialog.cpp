#include "adddialog.h"
#include "ui_adddialog.h"

AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog) {
    ui->setupUi(this);
    if(!allow_root) exit(0);
}

AddDialog::~AddDialog() {
    delete ui;
}

void AddDialog::on_pushButton_clicked() {
    QSqlQuery query;
    query.exec("SELECT * FROM users;");

    for(int i=0;query.next();i++) {
        if(query.value(1).toString() == ui->lineEdit->text()) {
            ui->label_3->setText("Такой пользователь уже существует");
            return;
        }
    }

    if(ui->lineEdit->text().trimmed() == "" || ui->lineEdit_2->text().trimmed() == "") {
        ui->label_3->setText("Введите логин и пароль");
        return;
    }

    QSqlQuery addquery;
    addquery.prepare("INSERT INTO users (name, password, root, help) VALUES(:name, :pass, 0, :hint);");
    addquery.bindValue(":name", ui->lineEdit->text());
    addquery.bindValue(":pass", ui->lineEdit_2->text());
    addquery.bindValue(":hint", ui->lineEdit_3->text());
    addquery.exec();
    close();
}

void AddDialog::on_pushButton_2_clicked() {
    close();
}
