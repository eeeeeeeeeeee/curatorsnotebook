#include "activity.h"
#include "ui_activity.h"

extern QSqlDatabase db;
extern int needUpdate;

Activity::Activity(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Activity) {
    ui->setupUi(this);

    QSqlQuery query;
    query.exec("SELECT * FROM students;");
    QSet<QString> groups;
    while(query.next()) {
        QString value = query.value(1).toString();
        groups.insert(value);
        qDebug() << value;
    }


    QSetIterator<QString> i(groups);
    while (i.hasNext()) {
        ui->comboBox->addItem(i.next());
    }
}

Activity::~Activity() {
    delete ui;
}

void Activity::on_pushButton_2_clicked() {
    close();
    delete this;
}

void Activity::on_pushButton_clicked() {
    QSqlQuery query;
    query.prepare("INSERT INTO activity_to_group(activity, student_group, activity_date, activity_descr) VALUES (:event, :group, :date, :descr);");
    query.bindValue(":event", ui->lineEdit->text());
    query.bindValue(":group", ui->comboBox->currentText());
    query.bindValue(":descr", ui->textEdit->toPlainText());
    query.bindValue(":date", ui->dateEdit->date().toString("dd.MM.yyyy"));
    query.exec();

    needUpdate = 1;

    close();
    delete this;
}
