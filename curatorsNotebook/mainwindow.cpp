﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

extern QSqlDatabase db;
extern int needUpdate;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    ui->pushButton->setVisible(0);

    ui->pushButton_2->setDisabled(1);
    ui->pushButton_4->setDisabled(1);
    ui->pushButton_5->setDisabled(1);

    //QTextCodec::setCodecForTr(QTextCodec::codecForName("Windows-1251"));

    //QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
       QTextCodec::setCodecForTr(codec);
       QTextCodec::setCodecForCStrings(codec);
       QTextCodec::setCodecForLocale(codec);




    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("../data/database.db");
    db.open();
    /*query.exec("SET NAMES utf8");


    QTextCodec *codec=QTextCodec::codecForName("utf8");
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);*/


    parseGroups();

    tmr = new QTimer();
    tmr->setInterval(1000);
    connect(tmr, SIGNAL(timeout()), this, SLOT(updateTime()));
    tmr->start();

}

void MainWindow::clearData() {
    ui->tableWidget->clearSelection();
    ui->listWidget_2->clear();
    ui->listWidget_4->clear();
    ui->label_9->setText("Фото");
    ui->tableWidget_3->clearSelection();
    ui->tableWidget_3->setRowCount(0);
    ui->tableWidget_2->clearSelection();
    ui->tableWidget_2->setRowCount(0);
}


void MainWindow::updateTime() {
    if(needUpdate) {

        clearData();
        ui->tableWidget->setRowCount(0);

        parseGroups();
        if(ui->listWidget->selectedItems().size()>0) {
            fillTableByGroup(ui->listWidget->selectedItems().at(0)->text());
            ui->pushButton_2->setDisabled(1);
            updateEvents();
        }

        needUpdate = 0;
    }
}

void MainWindow::parseGroups() {
    QSqlQuery query;
    query.exec("SELECT * FROM students;");
    QSet<QString> groups;
    while(query.next()) {
        QString value = query.value(1).toString();
        groups.insert(value);
        qDebug() << value;
    }


    qDebug() << "Проверка:";
    QSetIterator<QString> i(groups);
    while (i.hasNext()) {

        int allow = 1;
        QString content = i.next();
        for(int j=0;j<ui->listWidget->count();j++) {
            if(ui->listWidget->item(j)->text() == content) {
                allow = 0;
                break;
            }
            qDebug() << ui->listWidget->item(j)->text() << content;
        }

        if(allow) ui->listWidget->addItem(content);
    }
}


void MainWindow::fillSubjByGroup(QString group) {
    QSqlQuery query;
    query.prepare("SELECT * FROM subject_to_group WHERE student_group = :group;");
    query.bindValue(":group", group);
    query.exec();

    QSet<QString> groups;
    while(query.next()) {
        QString value = query.value(1).toString();
        groups.insert(value);
        qDebug() << value;
    }


    ui->listWidget_2->clear();

    QSetIterator<QString> i(groups);
    while (i.hasNext()) {
        ui->listWidget_2->addItem(i.next());
    }
    ui->listWidget_2->update();

}

void MainWindow::fillTableByGroup(QString group) {
    ui->pushButton_2->setDisabled(1);
    ui->pushButton_4->setDisabled(1);
    ui->pushButton_5->setDisabled(1);
    qDebug() << group;

    QSqlQuery query;
    QString q = "SELECT * FROM students WHERE student_group LIKE \"";
    q.append(group);
    q.append("\";");

    qDebug() << q;

    query.exec(q);
    qDebug() << query.executedQuery() << query.lastError();

    ui->tableWidget->setRowCount(0);

    ui->label_2->setText("Староста не найден");

    for(int i=0;query.next();i++) {
        qDebug() << query.value(0).toString();

        ui->tableWidget->insertRow(i);
        ui->tableWidget->setItem(i,0, new QTableWidgetItem(query.value(0).toString()));
        ui->tableWidget->setItem(i,1, new QTableWidgetItem(query.value(2).toString()));
        ui->tableWidget->setItem(i,2, new QTableWidgetItem(query.value(3).toString()));
        ui->tableWidget->setItem(i,3, new QTableWidgetItem(query.value(5).toString()));
        ui->tableWidget->setItem(i,4, new QTableWidgetItem(query.value(6).toString()));

        if(query.value(7).toInt()) {
            QString text = "Староста: ";
            text.append(query.value(2).toString());
            text.append("; Телефон: ");
            text.append(query.value(6).toString());

            ui->label_2->setText(text);
        }
    }
}

MainWindow::~MainWindow() {
    db.close();
    delete ui;
}

void MainWindow::on_listWidget_itemSelectionChanged() {
    clearData();
    updateEvents();

    fillTableByGroup(ui->listWidget->selectedItems().at(0)->text());
    fillSubjByGroup(ui->listWidget->selectedItems().at(0)->text());
    ui->pushButton_2->setDisabled(1);
    ui->pushButton_6->setEnabled(1);

}

void MainWindow::on_pushButton_2_clicked() {
    QSqlQuery query;

    QString group = ui->listWidget->selectedItems().at(0)->text();
    int row = ui->tableWidget->currentItem()->row();
    QString id = ui->tableWidget->item(row,0)->text();

    qDebug() << "INFO " << row << id;

    query.prepare("UPDATE students SET student_is_head = 0 WHERE student_group LIKE :group;");
    query.bindValue(":group", group);
    query.exec();
    qDebug() << query.lastQuery();

    query.prepare("UPDATE students SET student_is_head = 1 WHERE student_group LIKE :group AND student_id = :id");
    query.bindValue(":id", id);
    query.bindValue(":group", group);
    query.exec();
    qDebug() << query.lastQuery();

    fillTableByGroup(group);
}

void MainWindow::on_tableWidget_itemSelectionChanged() {
    ui->label_9->setText("Фото");

    ui->pushButton_2->setEnabled(1);
    ui->pushButton_4->setEnabled(1);
    ui->pushButton_5->setEnabled(1);

    int row = ui->tableWidget->currentItem()->row();
    QString id = ui->tableWidget->item(row,0)->text();

    QSqlQuery query;
    query.prepare("SELECT (student_photo_dir) FROM students WHERE student_id = :id");
    query.bindValue(":id", id);
    query.exec();

    if(!query.next()) return;
    if(query.value(0).toString() == "") return;

    QByteArray bytes = query.value(0).toByteArray();
    QPixmap pixmap;
    pixmap.loadFromData(bytes);
    ui->label_9->setPixmap(pixmap.scaled(ui->label_9->width(),ui->label_9->height(),Qt::KeepAspectRatio));
}

void MainWindow::on_listWidget_clicked(const QModelIndex &index) {
    ui->pushButton_3->setEnabled(1);
    ui->pushButton_2->setDisabled(1);
    ui->pushButton_4->setDisabled(1);
    ui->pushButton_5->setDisabled(1);
    ui->pushButton_9->setDisabled(1);
}

void MainWindow::on_pushButton_5_clicked() {
    QSqlQuery query;

    QString group = ui->listWidget->selectedItems().at(0)->text();
    int row = ui->tableWidget->currentItem()->row();
    QString id = ui->tableWidget->item(row,0)->text();

    qDebug() << "INFO " << row << id;

    query.prepare("DELETE FROM students WHERE student_group LIKE :group AND student_id = :id");
    query.bindValue(":id", id);
    query.bindValue(":group", group);
    query.exec();

    clearData();
    fillTableByGroup(group);
}

void MainWindow::on_pushButton_3_clicked() {
    AddStudent* w = new AddStudent();
    w->show();
    w->setType(1);
}

void MainWindow::on_pushButton_4_clicked() {
    AddStudent* w = new AddStudent();

    QString group = ui->listWidget->selectedItems().at(0)->text();
    int row = ui->tableWidget->currentItem()->row();
    QString id = ui->tableWidget->item(row,0)->text();

    w->setStudentId(id);
    w->setType(0);
    w->show();
}

void MainWindow::on_listWidget_2_itemSelectionChanged() {
    ui->pushButton_9->setDisabled(1);
    ui->tableWidget_2->clearSelection();
    ui->tableWidget_2->setRowCount(0);

    // Студенты
    QString group = ui->listWidget->selectedItems().at(0)->text();

    QSqlQuery query;
    query.prepare("SELECT (student_name) FROM students WHERE student_group = :group;");
    query.bindValue(":group", group);
    query.exec();

    QSet<QString> groups;
    while(query.next()) {
        QString value = query.value(0).toString();
        groups.insert(value);
        qDebug() << value;
    }


    ui->listWidget_4->clear();

    QSetIterator<QString> j(groups);
    while (j.hasNext()) {
        ui->listWidget_4->addItem(j.next());
    }
    ui->listWidget_4->update();

    // Лист оценок
    if( !(ui->listWidget_2->selectedItems().size()>0)) return;

    QString item = ui->listWidget_2->selectedItems().at(0)->text();

    query.prepare("SELECT * FROM subject_journal WHERE subject LIKE :item;");
    query.bindValue(":item", item);
    query.exec();

    QMap<QString, int> map;
    QMap<QString, int> maplen;


    for(int i=0;query.next();i++) {
        qDebug() << query.value(0).toString();

        int value = query.value(4).toInt();
        map.insert(query.value(2).toString(), map.value(query.value(2).toString(), 0) + value);
        maplen.insert(query.value(2).toString(),maplen.value(query.value(2).toString(),0) + 1);

        /*ui->tableWidget_3->insertRow(i);
        ui->tableWidget_3->setItem(i,0, new QTableWidgetItem(query.value(3).toString()));
        ui->tableWidget_3->setItem(i,1, new QTableWidgetItem(query.value(4).toString()));*/

    }

    int i = 0;
    QMap <QString,int>::iterator it;
    for (it = map.begin(); it != map.end(); ++it) {
        double value = (double)it.value() / maplen.value(it.key(), 1);
        ui->tableWidget_2->insertRow(i);
        ui->tableWidget_2->setItem(i,0, new QTableWidgetItem(it.key()));
        ui->tableWidget_2->setItem(i,1, new QTableWidgetItem(QString::number(value)));
        i++;
    }
}


void MainWindow::fillMark() {
    ui->pushButton_9->setEnabled(1);

    if( !(ui->listWidget_4->selectedItems().size()>0 && ui->listWidget_2->selectedItems().size()>0)) return;

    QString name = ui->listWidget_4->selectedItems().at(0)->text();
    QString item = ui->listWidget_2->selectedItems().at(0)->text();

    QSqlQuery query;
    query.prepare("SELECT * FROM subject_journal WHERE student_name LIKE :name AND subject LIKE :item;");
    query.bindValue(":name", name);
    query.bindValue(":item", item);
    query.exec();

    ui->tableWidget_3->setRowCount(0);

    qDebug() << name << query.lastError();

    for(int i=0;query.next();i++) {
        qDebug() << query.value(0).toString();

        ui->tableWidget_3->insertRow(i);
        ui->tableWidget_3->setItem(i,0, new QTableWidgetItem(query.value(3).toString()));
        ui->tableWidget_3->setItem(i,1, new QTableWidgetItem(query.value(4).toString()));

    }

}

void MainWindow::on_listWidget_4_itemSelectionChanged() {
    ui->tableWidget_3->clearSelection();
    ui->tableWidget_3->setRowCount(0);
    fillMark();
}

void MainWindow::on_pushButton_9_clicked() {
    QSqlQuery query;
    query.prepare("INSERT INTO subject_journal (subject, student_name, subject_date, subject_mark) VALUES (:subj, :name, :date, :mark);");
    query.bindValue(":subj", ui->listWidget_2->selectedItems().at(0)->text());
    query.bindValue(":name", ui->listWidget_4->selectedItems().at(0)->text());
    query.bindValue(":date", QDate::currentDate().toString());
    query.bindValue(":mark", QString::number(ui->spinBox->value()));
    query.exec();

    fillMark();
}

void MainWindow::on_pushButton_6_clicked() {
    if(!(ui->listWidget->selectedItems().size()>0)) return;

    QSqlQuery query;
    query.prepare("INSERT INTO subject_to_group (subject, student_group) VALUES (:subj, :group);");
    query.bindValue(":subj", ui->lineEdit->text());
    query.bindValue(":group", ui->listWidget->selectedItems().at(0)->text());
    query.exec();

    clearData();
    fillSubjByGroup(ui->listWidget->selectedItems().at(0)->text());
}

void MainWindow::on_pushButton_8_clicked() {
    Activity* w = new Activity();
    w->show();


}

void MainWindow::updateEvents() {
    ui->label_8->setText("Название события");
    ui->textEdit->setText("");
    if(!(ui->listWidget->selectedItems().size()>0)) return;

    QString group = ui->listWidget->selectedItems().at(0)->text();

    QSqlQuery query;
    query.prepare("SELECT * FROM activity_to_group WHERE student_group LIKE :group;");
    query.bindValue(":group",group);
    query.exec();

    ui->listWidget_3->clearSelection();
    ui->listWidget_3->clear();

    while(query.next()) {
        ui->listWidget_3->addItem(query.value(1).toString());
    }
}

void MainWindow::on_listWidget_3_itemSelectionChanged() {
    if(!(ui->listWidget->selectedItems().size()>0 && ui->listWidget_3->selectedItems().size()>0)) return;

    QString group = ui->listWidget->selectedItems().at(0)->text();
    QString name = ui->listWidget_3->selectedItems().at(0)->text();


    QSqlQuery query;
    query.prepare("SELECT * FROM activity_to_group WHERE student_group LIKE :group AND activity LIKE :name;");
    query.bindValue(":group",group);
    query.bindValue(":name",name);
    query.exec();

    if(query.next()) {
        ui->label_8->setText(query.value(1).toString());
        ui->textEdit->setText(query.value(4).toString());
        ui->dateEdit->setDate(QDate::fromString(query.value(3).toString(),"dd.MM.yyyy"));
    }
}
