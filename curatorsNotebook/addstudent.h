#ifndef ADDSTUDENT_H
#define ADDSTUDENT_H

#include <QWidget>
#include <QDate>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QPixmap>
#include <QFile>
#include <QFileDialog>
#include <QBuffer>

namespace Ui {
    class AddStudent;
}

class AddStudent : public QWidget
{
    Q_OBJECT

public:
    explicit AddStudent(QWidget *parent = 0);
    void setStudentId(QString id);
    void setType(int type);
    ~AddStudent();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_AddStudent_destroyed();

    void on_pushButton_3_clicked();

private:
    Ui::AddStudent *ui;
    QString photo;
};

#endif // ADDSTUDENT_H
