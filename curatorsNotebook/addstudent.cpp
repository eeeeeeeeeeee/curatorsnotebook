﻿#include "addstudent.h"
#include "ui_addstudent.h"

extern QSqlDatabase db;
extern int needUpdate;

int type = 0;
QString sid;

AddStudent::AddStudent(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddStudent) {
    ui->setupUi(this);

    photo = "";
}

void AddStudent::setType(int t) {
    type = t;
    if(t>0) {
        ui->pushButton->setText("Добавить");
        ui->pushButton_3->setDisabled(1);
    } else ui->pushButton_3->setEnabled(1);
    ui->pushButton->update();
}

void AddStudent::setStudentId(QString id) {
    sid = id;

    QSqlQuery query;
    QString q = "SELECT * FROM students WHERE student_id = ";
    q.append(id);
    q.append(";");

    qDebug() << q;

    query.exec(q);
    qDebug() << query.executedQuery() << query.lastError();

    if(query.next()) {
        ui->lineEdit->setText(query.value(1).toString());
        ui->lineEdit_2->setText(query.value(2).toString());

        QString date = query.value(3).toString();
        QDate d = QDate::fromString(date, "dd.MM.yyyy");
        ui->dateEdit->setDate(d);

        ui->lineEdit_3->setText(query.value(5).toString());
        ui->lineEdit_4->setText(query.value(6).toString());

        QByteArray bytes = query.value(4).toByteArray();
        QPixmap pixmap;
        pixmap.loadFromData(bytes);
        ui->label_6->setPixmap(pixmap.scaled(ui->label_6->width(),ui->label_6->height(),Qt::KeepAspectRatio));
    }
}

AddStudent::~AddStudent() {
    delete ui;
}

void AddStudent::on_pushButton_2_clicked() {
    close();
    delete this;
}

void AddStudent::on_pushButton_clicked() {
    if(type<1) {
        QSqlQuery query;
        query.prepare("UPDATE students SET student_name = :name, student_group = :group, birth_date = :date, student_email = :mail, student_phone = :phone WHERE student_id = :id;");
        query.bindValue(":group", ui->lineEdit->text());
        query.bindValue(":name", ui->lineEdit_2->text());
        query.bindValue(":date", ui->dateEdit->text());
        query.bindValue(":mail", ui->lineEdit_3->text());
        query.bindValue(":phone", ui->lineEdit_4->text());
        query.bindValue(":id", sid);
        query.exec();

        qDebug() << query.executedQuery() << query.lastError();
        qDebug() << ui->dateEdit->text();
    } else {
        QSqlQuery query;
        query.prepare("INSERT INTO students (student_group, student_name, birth_date, student_photo_dir, student_email, student_phone) VALUES (:group, :name, :date, :photo, :mail, :phone);");
        query.bindValue(":group", ui->lineEdit->text());
        query.bindValue(":name", ui->lineEdit_2->text());
        query.bindValue(":date", ui->dateEdit->text());
        query.bindValue(":mail", ui->lineEdit_3->text());
        query.bindValue(":phone", ui->lineEdit_4->text());
        query.bindValue(":photo", photo);
        query.exec();

        qDebug() << query.executedQuery() << query.lastError();
        qDebug() << ui->dateEdit->text();
    }

    needUpdate = 1;
    close();
    delete this;
}

void AddStudent::on_AddStudent_destroyed() {

}

void AddStudent::on_pushButton_3_clicked() {
    QString file = "";
    file = QFileDialog::getOpenFileName(this, "Загрузить фото", "", "*.png");
    if(file == "") return;

    QPixmap myPixmap( file );
    ui->label_6->setPixmap( myPixmap );

    QByteArray bytes;
    QBuffer buffer(&bytes);
    buffer.open(QIODevice::WriteOnly);
    myPixmap.save(&buffer, "PNG");

    QString str = bytes;

    if(type==0) {
        QSqlQuery query;
        query.prepare("UPDATE students SET student_photo_dir = :png WHERE student_id = :id");
        query.bindValue(":id", sid);
        query.bindValue(":png", bytes);
        query.exec();
    } else {
        photo = str;
    }

}
