#ifndef ACTIVITY_H
#define ACTIVITY_H

#include <QWidget>
#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSet>
#include <QSetIterator>
#include <qDebug>
#include <QTextCodec>
#include <QSqlError>
#include <QObject>
#include <QModelIndex>
#include <QDialog>
#include <QTimer>
#include <QTime>
#include <QByteArray>
#include <QPixmap>
#include <QDate>
#include <QMap>

namespace Ui {
    class Activity;
}

class Activity : public QWidget
{
    Q_OBJECT

public:
    explicit Activity(QWidget *parent = 0);
    ~Activity();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::Activity *ui;
};

#endif // ACTIVITY_H
