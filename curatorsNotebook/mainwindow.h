#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSet>
#include <QSetIterator>
#include <qDebug>
#include <QTextCodec>
#include <QSqlError>
#include <QObject>
#include <QModelIndex>
#include <QDialog>
#include <QTimer>
#include <QTime>
#include <QByteArray>
#include <QPixmap>
#include <QDate>
#include <QMap>

#include "addstudent.h"
#include "activity.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_listWidget_itemSelectionChanged();

    void on_pushButton_2_clicked();

    void on_tableWidget_itemSelectionChanged();

    void on_listWidget_clicked(const QModelIndex &index);

    void on_pushButton_5_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void updateTime();
    void on_listWidget_2_itemSelectionChanged();

    void on_listWidget_4_itemSelectionChanged();

    void on_pushButton_9_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_8_clicked();

    void on_listWidget_3_itemSelectionChanged();

private:
    Ui::MainWindow *ui;
    QTimer *tmr;
    void fillTableByGroup(QString group);
    void fillSubjByGroup(QString group);
    void fillMark();
    void parseGroups();
    void clearData();
    void updateEvents();
};

#endif // MAINWINDOW_H
